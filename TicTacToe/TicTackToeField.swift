import UIKit

enum Symbol: String {
    case x     = "X"
    case o     = "0"
    case empty = " "
}

class TicTackToeField {
    
    var field          = [[UIButton]]()
    var symbol: Symbol = .x
    let fieldStackView: UIStackView
    let size: Int
    let namePlyaerOne: String
    let namePlyaerTwo: String
    let playerMoveLabel: UILabel
    let vc: UIViewController
        
    init(size: Int, fieldStackView: UIStackView, namePlyaerOne: String, namePlyaerTwo: String, playerMoveLabel: UILabel, vc: UIViewController) {
        
        self.size            = size
        self.fieldStackView  = fieldStackView
        self.namePlyaerOne   = namePlyaerOne
        self.namePlyaerTwo   = namePlyaerTwo
        self.playerMoveLabel = playerMoveLabel
        self.vc              = vc
        
        self.playerMoveLabel.text = "Ход \(namePlyaerOne) (X)"
        
        for _ in 0..<size {
            
            let stackView = UIStackView()
            fieldStackView.addArrangedSubview(stackView)
            
            stackView.axis         = .horizontal
            stackView.distribution = .fillEqually
            stackView.spacing      = 5
            
            addButtonsToStackView(numberOfButtons: size, stackView: stackView)
        }
    }
    
    func addButtonsToStackView(numberOfButtons: Int, stackView: UIStackView) {
        var row = [UIButton]()
        
        for _ in 1...numberOfButtons {
            
            let button = UIButton()
            button.setTitle(Symbol.empty.rawValue, for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.backgroundColor = .white
            button.titleLabel?.font = UIFont.systemFont(ofSize: 60)
            
            button.addAction(
                UIAction(
                    handler: { [self]  _ in
                        
                        if button.titleLabel?.text == Symbol.empty.rawValue {
                            
                            if self.symbol == .x {
                                button.setTitle(Symbol.x.rawValue, for: .normal)
                                self.symbol = .o
                                self.playerMoveLabel.text = "Ход \(namePlyaerTwo) (O)"
                            } else {
                                button.setTitle(Symbol.o.rawValue, for: .normal)
                                self.symbol = .x
                                self.playerMoveLabel.text = "Ход \(namePlyaerOne) (X)"
                            }
                            
                            if let symbol = win() {
                                self.playerMoveLabel.textColor = .purple
                                if symbol == Symbol.x.rawValue {
                                    self.playerMoveLabel.text = "!!!Победил \(namePlyaerOne) (X)!!!"
                                } else {
                                    self.playerMoveLabel.text = "!!!Победил игрок \(namePlyaerTwo) (0)!!!"
                                }
                                alert(title: "Игра окончена", message: "Сыграть снова?")
                            }
                            
                            if gameFinish() {
                                self.playerMoveLabel.text = "!!!Ничья!!!"
                                alert(title: "Игра окончена", message: "Сыграть снова?")
                            }
                        }
                    }),
                for: .touchUpInside)
            
            stackView.addArrangedSubview(button)
            row.append(button)
        }
        
        field.append(row)
    }
    
    func win() -> String? {
        
        return winLine()
        ?? winColumn()
        ?? winDiagonal()
        ?? winReverseDiagonal()
    }
    
    func winLine() -> String? {
        
        for row in 0..<size {
            
            let firstSymbol = field[row][0].titleLabel?.text ?? ""
            var isWin = true
            for cell in 0..<size {
                let cel = field[row][cell].titleLabel?.text ?? ""
                if cel != firstSymbol || firstSymbol == Symbol.empty.rawValue {
                    isWin = false
                    break
                }
            }
            if isWin {
                for cell in 0..<size {
                    field[row][cell].backgroundColor = .green
                }
                return firstSymbol
                 
            }
        }
        return nil
    }
    
    func winColumn() -> String? {
        
        for row in 0..<size {
            
            let firstSymbol = field[0][row].titleLabel?.text ?? ""
            var isWin = true
            for cell in 1..<size {
                let cel = field[cell][row].titleLabel?.text ?? ""
                if cel != firstSymbol || firstSymbol == Symbol.empty.rawValue {
                    isWin = false
                    break
                }
            }
            if isWin == true {
                for cell in 0..<size {
                    field[cell][row].backgroundColor = .green
                }
                return firstSymbol
            }
        }
        return nil
    }
    
    func winDiagonal() -> String? {
        
        let firstSymbol = field[0][0].titleLabel?.text ?? ""
        
        for row in 0..<size {
            let cel = field[row][row].titleLabel?.text ?? ""
            if cel != firstSymbol || firstSymbol == Symbol.empty.rawValue {
                return nil
            }
        }
        for cell in 0..<size {
            field[cell][cell].backgroundColor = .green
        }
        return firstSymbol
    }
    
    func winReverseDiagonal() -> String? {
        
        let firstSymbol = field[0][size - 1].titleLabel?.text ?? ""
        
        for row in 1..<size {
            let cel = field[row][size - row - 1].titleLabel?.text ?? ""
            if cel != firstSymbol || firstSymbol == Symbol.empty.rawValue {
                return nil
            }
        }
        for cell in 0..<size {
            field[cell][size - cell - 1].backgroundColor = .green
        }
        return firstSymbol
    }
    
    func gameFinish() -> Bool {

        for row in field {
            for cell in row {
                if cell.titleLabel?.text == Symbol.empty.rawValue {
                    return false
                }
            }
        }
        for row in field {
            for cell in row {
                cell.backgroundColor = .darkGray
            }
        }
        return true
    }
    
    func alert(title: String, message: String) {
        let alert = AlertWindow.createAlert(title: title, message: message, style: .actionSheet)
        vc.present(alert, animated: true)
        
        let actionYes = UIAlertAction(title: "да", style: .default) { [self] _ in
            
            symbol = .x
            playerMoveLabel.text = "Ход \(namePlyaerOne) (X)"
            playerMoveLabel.textColor = .black
            
            for row in field {
                for cell in row {
                    cell.setTitle(Symbol.empty.rawValue, for: .normal)
                    cell.backgroundColor = .white
                }
            }
        }
        
        let actionNo = UIAlertAction(title: "нет", style: .default) { [self] _ in
            vc.navigationController?.popViewController(animated: true)
        }
        
        alert.addAction(actionYes)
        alert.addAction(actionNo)
    }
}


