import UIKit

class ViewControllerTwo: UIViewController {
    
    @IBOutlet weak var fieldStackView: UIStackView!
    @IBOutlet weak var playerMoveLabel: UILabel!
    
    var namePlyaerOne = "Игрок 1"
    var namePlayerTwo = "Игрок 2"
    var fieldSize     = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        game()
    }
    
    func game() {
        TicTackToeField(size: fieldSize, fieldStackView: fieldStackView, namePlyaerOne: namePlyaerOne, namePlyaerTwo: namePlayerTwo, playerMoveLabel: playerMoveLabel, vc: self)
    }
}

extension ViewControllerTwo: DelegateDataGame {
    
    func nameOne(name: String) {
        namePlyaerOne = name
    }
    
    func nameTwo(name: String) {
        namePlayerTwo = name
    }
    
    func fieldSize(size: Int)  {
        fieldSize     = size
    }
    
        
}
