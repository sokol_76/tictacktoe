import UIKit

class AlertWindow {
    
    static func createAlert(title: String, message: String, style: UIAlertController.Style) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        
        return alert
    }
   
    

}

