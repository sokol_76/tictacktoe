import UIKit

protocol DelegateDataGame {
    func nameOne(name: String)
    func nameTwo(name: String)
    func fieldSize(size: Int)
}

class ViewController: UIViewController {
    
    var delegate: DelegateDataGame?
    
    @IBOutlet weak var nameOneTextField: UITextField!
    @IBOutlet weak var nameTwoTextField: UITextField!
    @IBOutlet weak var fieldSizePickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fieldSizePickerView.dataSource = self
        fieldSizePickerView.delegate   = self
        fieldSizePickerView.selectRow(1, inComponent: 0, animated: true)
    }
    
    @IBAction func exitGameButton(_ sender: UIButton) {
        
        let alert = AlertWindow.createAlert(title: "Выход из программы", message: "Вы уверены, что хотите покинуть программу?", style: .alert)
        
        let actionYes = UIAlertAction(title: "да", style: .default) { _ in
            exit(0) }
        
        let actionNo = UIAlertAction(title: "нет", style: .default)
        
        alert.addAction(actionYes)
        alert.addAction(actionNo)
        self.present(alert, animated: true)
    }
    
    @IBAction func startGameButton(_ sender: UIButton) {
        
        if nameOneTextField.text  == "" || nameTwoTextField.text == "" {
            
            let alert = AlertWindow.createAlert(title: "Предупреждение", message: "Введите имена игроков", style: .alert)
            
            let actionOk = UIAlertAction(title: "ок", style: .default)

            alert.addAction(actionOk)
            self.present(alert, animated: true)
            
        }
        
        if nameOneTextField.text == nameTwoTextField.text {
            
            
            let actionOk = UIAlertAction(title: "ок", style: .default)
            let alert    = AlertWindow.createAlert(title: "Предупреждение", message: "Имена игроков должны отличаться", style: .alert)
            alert.addAction(actionOk)
            self.present(alert, animated: true)
            
        }
    
        let vc        = storyboard?.instantiateViewController(withIdentifier: "GameVC") as! ViewControllerTwo
        self.delegate = vc
        
        delegate?.fieldSize(size: fieldSizePickerView.selectedRow(inComponent: 0) + 2)
        delegate?.nameOne(name: nameOneTextField.text!)
        delegate?.nameTwo(name: nameTwoTextField.text!)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension ViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
}

extension ViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 2) x \(row + 2)"
    }
}

